/*
 * Copyright Draegerwerk AG & Co. KGaA
 *
 * This header provides an interface for direct access to the IPC memory from within
 * the vxWorks kernel. The implementations differ for both processors.
 *
 */

#ifndef BSP_SW_PKGS_OS_PLATFORM_M48COMMON_1_0_0_0_H_M48RAWIPCBASE_H_
#define BSP_SW_PKGS_OS_PLATFORM_M48COMMON_1_0_0_0_H_M48RAWIPCBASE_H_

#include <vxWorks.h>

#define MAX_IPC_UNITS 2

#ifdef __cplusplus
extern "C" {
#endif


/** write data stored in buffer to IPC channel 0/1 at address offset (range 0...getIpcBufferSize (int channel)-length) */
extern STATUS writeIpcBuffer(int channel, unsigned int offset, void* buffer, unsigned int length);
/** read length bytes of data from IPC channel 0/1 at address offset */
extern STATUS readIpcBuffer (int channel, unsigned int offset, void* buffer, unsigned int length);
/** Write total size of IPC buffer channel 0/1. (typically 4kb)*/
extern UINT32 getIpcBufferSize (int channel);
/** Return TRUE if a driver for this channel is attached **/
extern BOOL isIpcBufferInitialized(int channel);

#ifdef __cplusplus
}
#endif

#endif /* BSP_SW_PKGS_OS_PLATFORM_M48COMMON_1_0_0_0_H_M48RAWIPCBASE_H_ */
