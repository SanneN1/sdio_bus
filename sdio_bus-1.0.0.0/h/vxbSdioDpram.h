/* vxbSdioDpram.h - SDIO DPRAM driver header file for vxBus */

/*
 * Copyright (c) 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
10jul20,vpo  written
*/

#ifndef __INCvxbSdioDpramh
#define __INCvxbSdioDpramh

#ifdef __cplusplus
    extern "C" {
#endif /* __cplusplus */

#define SDIO_DPRAM_NAME  "sdioDpram"

/* DPRAM driver control */

typedef struct sdioDpramDrvCtrl
    {
    VXB_DEV_HANDLE  pInst;
    void *          ramHandle;
    UINT32          ramBase;
    UINT32          ramSize;
    int             unit;
    } SDIO_DPRAM_CTRL;


STATUS dpramRead (int unit, UINT32  dpramAddr, UINT8  *buffer, UINT16  len);
STATUS dpramWrite (int unit, UINT32  dpramAddr, UINT8  *buffer, UINT16  len);
    
#ifdef __cplusplus
        }
#endif /* __cplusplus */

#endif /* __INCvxbSdioh */
