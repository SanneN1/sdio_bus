/* vxbSdio.h - SDIO driver header file for vxBus */

/*
 * Copyright (c) 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
05nov15,cis  Added fix for trace GER-DRA12980 B0004 SDIO bus collisions
10jul15,vpo  written
*/

#ifndef __INCvxbSdioh
#define __INCvxbSdioh

#ifdef __cplusplus
    extern "C" {
#endif /* __cplusplus */

BUSTYPE_DECL(vxbSdioBus);
#define VXB_BUSID_SDIO     BUSTYPE_ID(vxbSdioBus) /* Mux class device */

/* forward declarations */

#define SDIO_NAME           "sdioBus"

#define MAX_SDIO_BUSES      2

#define SDIO_BLOCK_SIZE		128

typedef struct
	{
	FUNCPTR	       busAccess;
	VXB_DEV_HANDLE pdev;
	} VXB_SDIO_ACCESS;


/**
 *Define the SDIO CCCR structure.
 */
typedef struct
    {
	UINT8 cccr_sdio_revision;
	UINT8 sd_specification_revision;
	UINT8 io_enable;
	UINT8 io_ready;
	UINT8 int_enable;
	UINT8 int_pending;
	UINT8 io_abort;
	UINT8 bus_interface_control;
	UINT8 card_capability;
	UINT8 common_cis_pointer_low;
	UINT8 common_cis_pointer_middle;
	UINT8 common_cis_pointer_high;
	UINT8 bus_suspend;
	UINT8 function_select;
	UINT8 exec_flags;
	UINT8 ready_flags;
	UINT8 fn0_block_size_low;
	UINT8 fn0_block_size_high;
	UINT8 power_control;
	UINT8 bus_speed_select;
	UINT8 uhs_i_support;
	UINT8 driver_strength;
	UINT8 interrupt_extension;
	} SDIO_CCCR;

/*SDIO FBR define*/
typedef struct
	{
	UINT8 stardard_if_code;
	UINT8 ext_standard_if_code;
	UINT8 power_control;
	UINT32  pointerOfCIS;
	UINT32  pointerOfCSA;
	UINT8 dataAccessWindowOfCSA;
	UINT16  blockSize;
	} SDIO_FBR;

typedef struct sdioInfo
    {
	UINT16 maxBlockSizeFunc0;
	UINT16 maxBlockSize;
	BOOL   supportHighSpeed;
	UINT8  busSpeedSelect;
	BOOL   supportBlockMode;
	BOOL   support4Bit;

	SDIO_CCCR      cccr;
    } SDIO_INFO;

typedef struct sdioCtrl
    {
    VXB_DEV_HANDLE  pInst;              /* device handler   */
    SDIO_INFO      *pInfo;              /* detailed information */
	VXB_FDT_DEV    *pParentFdtDev;
	UINT32          idx;                /* card index */
    UINT32          tranSpeed;          /* transfer speed */
    BOOL            highSpeed;          /* 50 MHz */
    UINT32          blkLen;             /* read/write block length per SDIO function*/
	UINT16 			maxNumBlock;        /* max. number in blocks in block mode */
	BOOL 			dat4Bit;
    SEM_ID          sdioAccessSem;

    } SDIO_CTRL;

typedef struct sdiodevInfo
    {
	struct vxbFdtDev  vxbFdtDev;
	VXB_RESOURCE_LIST vxbResList;
    } SDIO_DEV_INFO;

#ifdef __cplusplus
        }
#endif /* __cplusplus */

#endif /* __INCvxbSdioh */
