/* vxbAlteraAddrExtender.h -  Address span extender driver header file for vxBus */

/*
 * Copyright (c) 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
10jul23,vpo  written
*/

#ifndef __INCvxbAlteraAddrExtenderh
#define __INCvxbAlteraAddrExtenderh

#ifdef __cplusplus
    extern "C" {
#endif /* __cplusplus */

#define ADDR_EXTENDER_NAME   "addrExtender"

typedef struct adrExtenderDrvCtrl
    {
    VXB_DEV_ID   pInst;
    void *       cntlHandle;
    UINT32       cntlBase;
    void *       windHandle;
    UINT32       windBase;
	UINT32       windSize;
    
    UINT32       curWindBase;
    } ADDR_EXTENDER_CTRL;
    
typedef struct adrExtenderDevInfo {
    struct vxbFdtDev vxbFdtDev;
    VXB_RESOURCE_LIST vxbResList;
} ADDR_EXTENDER_DEV_INFO;

#ifdef __cplusplus
        }
#endif /* __cplusplus */

#endif /* __INCvxbSdioh */