/* 40vxbSdioBus.cdf - SDIO Bus Driver */

/*
 * Copyright (c) 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
10jul15,vpo  written
*/

Component DRV_SDIO_BUS {
    NAME        SDIO bus driver
    SYNOPSIS    SDIO bus driver
    _CHILDREN   FOLDER_SDMMC 
    LINK_SYMS   vxbSdioDrv
    REQUIRES    INCLUDE_VXBUS  \
                INCLUDE_SD_BUS
}
