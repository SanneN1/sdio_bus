/* 40vxbAltAvalonMem.cdf - Altera Avalon Memory Driver over SDIO */

/*
 * Copyright (c) 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
10jul15,vpo  written
*/

Component DRV_SDIO_ALT_AVALON_MEM {
    NAME        Altera Avalon Memory Driver over SDIO
    SYNOPSIS    Altera Avalon Memory Driver over SDIO
    _CHILDREN   FOLDER_SDMMC 
    LINK_SYMS   vxbSdioDpramDrv
    REQUIRES    DRV_SDIO_BUS
    INIT_AFTER  DRV_SDIO_BUS
}
