/* vxbAlteraAddrExtender.c - Address span extender driver file */

/*
 * Copyright (c) 2015, 2016 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
25feb16,hni  fixed PC-lint warnings (GER-DRA12980)
24feb16,cis  code review modifications (GER-DRA12980 TRACE A0051)
10jul15,vpo  written
*/

/*
DESCRIPTION
This module implements a driver for the Altera Address Span Extender attached
to the SDIO bus.

\ie
SEE ALSO: vxBus
*/

/* includes */
#include <vxWorks.h>
#include <stdio.h>

#include <hwif/vxBus.h>
#include <hwif/buslib/vxbFdtLib.h>

#include <vxbSdMmcLib.h>
#include <vxbSdio.h>
#include <vxbAlteraAddrExtender.h>

/*Enable debug info*/

#undef ADDR_EXTENDER_DBG_ON
#ifdef  ADDR_EXTENDER_DBG_ON

#include <private/kwriteLibP.h>         /* _func_kprintf */
#ifdef  LOCAL
#undef  LOCAL
#define LOCAL
#endif

#define ADDR_EXTENDER_DBG_INIT           0x00000001
#define ADDR_EXTENDER_DBG_RW             0x00000002
#define ADDR_EXTENDER_DBG_ERR            0x00000004
#define ADDR_EXTENDER_DBG_INFO           0x00000008
#define ADDR_EXTENDER_DBG_ALL            0xffffffff
#define ADDR_EXTENDER_DBG_OFF            0x00000000

LOCAL UINT32 addrExtenderDbgMask = ADDR_EXTENDER_DBG_ALL;

#define ADDR_EXTENDER_DBG(mask, string, a, b, c, d, e, f)           \
    if ((addrExtenderDbgMask & mask) || (mask == ADDR_EXTENDER_DBG_ALL))  \
        if (_func_kprintf != NULL) \
           (* _func_kprintf)(string, a, b, c, d, e, f)
#else
#   define ADDR_EXTENDER_DBG(mask, string, a, b, c, d, e, f)
#endif  /* ADDR_EXTENDER_DBG_ON */


LOCAL STATUS addrExtenderProbe (VXB_DEV_ID);
LOCAL STATUS addrExtenderAttach (VXB_DEV_ID);
LOCAL VXB_RESOURCE * addrExtenderResAlloc (VXB_DEV_ID, VXB_DEV_ID, UINT32);
LOCAL STATUS addrExtenderResFree (VXB_DEV_ID, VXB_DEV_ID, VXB_RESOURCE * pRes);
LOCAL VXB_RESOURCE_LIST * addrExtenderResListGet(VXB_DEV_ID, VXB_DEV_ID);
LOCAL VXB_FDT_DEV * addrExtenderDevInfo (VXB_DEV_ID, VXB_DEV_ID);

LOCAL UINT32 addrExtenderAccess    (UINT32, UINT32, UINT32);

/* Suppress clang-tidy message. This initialization action is provided by VxWorks. We cannot change it. */
/* NOLINTNEXTLINE(cppcoreguidelines-interfaces-global-init) */
LOCAL VXB_DRV_METHOD vxbAddrExtender_methods[] =
    {
    { VXB_DEVMETHOD_CALL(vxbDevProbe), addrExtenderProbe },
    { VXB_DEVMETHOD_CALL(vxbDevAttach), addrExtenderAttach },

    { VXB_DEVMETHOD_CALL (vxbResourceAlloc),   (FUNCPTR)addrExtenderResAlloc },
    { VXB_DEVMETHOD_CALL (vxbResourceFree),    (FUNCPTR)addrExtenderResFree },
    { VXB_DEVMETHOD_CALL (vxbResourceListGet),
        (FUNCPTR)addrExtenderResListGet },
    { VXB_DEVMETHOD_CALL (vxbFdtDevGet),       (FUNCPTR)addrExtenderDevInfo },

    VXB_DEVMETHOD_END
    };

LOCAL VXB_FDT_DEV_MATCH_ENTRY fdtAddrExtenderMatch[] =
    {
        {
        "alt,avalon_span_extender",     /* compatible */
        NULL                            /* no configuration */
        },
        {}                              /* empty terminated list */
    };

/* Address Span Extender  openfirmware driver */

VXB_DRV vxbAddrExtenderDrv =
    {
    { NULL } ,
    ADDR_EXTENDER_NAME,      /* Name */
    "Address Extender driver",  /* Description */
	VXB_BUSID_FDT,           /* Class */
    0,                       /* Flags */
    0,                       /* Reference count */
    vxbAddrExtender_methods  /* Method table */
    };

VXB_DRV_DEF(vxbAddrExtenderDrv)

LOCAL ADDR_EXTENDER_CTRL *gAddrExtenderDrvCtrl = NULL;

/*******************************************************************************
*
* addrExtenderProbe - vxbus probe function
*
* This function is called by vxBus to probe device.
*
* RETURNS: OK if the assumed cevice is a valid (or compatible) Altera Memory
* Extender. ERROR otherwise.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS addrExtenderProbe
    (
    VXB_DEV_HANDLE pDev
    )
    {
    return vxbFdtDevMatch (pDev, fdtAddrExtenderMatch, NULL);
    }

/*******************************************************************************
*
* addrExtenderAttach - VxBus attach
*
*
* RETURNS: OK or ERROR when initialization failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS addrExtenderAttach
    (
    VXB_DEV_HANDLE pDev
    )
    {
    VXB_FDT_DEV             *pFdtDev;
    VXB_FDT_DEV             *pNewFdtDev;
    VXB_RESOURCE            *pRes0 = NULL;
    VXB_RESOURCE            *pRes1 = NULL;
    ADDR_EXTENDER_CTRL      *pDrvCtrl;
    VXB_DEV_ID              pCur;
    ADDR_EXTENDER_DEV_INFO  *pAddrExtenderDevInfo;
    int                     offset;


    pDrvCtrl = (ADDR_EXTENDER_CTRL *) vxbMemAlloc (sizeof (ADDR_EXTENDER_CTRL));
    if (pDrvCtrl == NULL)
        return ERROR;
    vxbDevSoftcSet(pDev, pDrvCtrl);
    pDrvCtrl->pInst = pDev;
    gAddrExtenderDrvCtrl = pDrvCtrl;

    /* cntl */

    pRes0 = vxbResourceAlloc (pDev, VXB_RES_MEMORY, 0);
    if ((pRes0 == NULL) || (pRes0->pRes == NULL))
       goto error;

    pDrvCtrl->cntlHandle = ((VXB_RESOURCE_ADR *) (pRes0->pRes))->pHandle;
    pDrvCtrl->cntlBase = (UINT32)((VXB_RESOURCE_ADR *)(pRes0->pRes))->virtual;

    /* windowed_slave */

    pRes1 = vxbResourceAlloc (pDev, VXB_RES_MEMORY, 1);
    if ((pRes1 == NULL) || (pRes1->pRes == NULL))
       goto error;

    pDrvCtrl->windHandle = ((VXB_RESOURCE_ADR *) (pRes1->pRes))->pHandle;
    pDrvCtrl->windBase = (UINT32)((VXB_RESOURCE_ADR *)(pRes1->pRes))->virtual;
    pDrvCtrl->windSize = (UINT32)((VXB_RESOURCE_ADR *)(pRes1->pRes))->size;

    /* Check that windSize is a power of 2 */

    if (pDrvCtrl->windSize & (pDrvCtrl->windSize - 1))
        {
        (void)printf("Incorrect windowed_slave size (0x%x) for Altera Address"
            " Span Extender\n", pDrvCtrl->windSize);
        goto error;
        }

    ADDR_EXTENDER_DBG(ADDR_EXTENDER_DBG_INIT,
    		"addrExtenderAttach: ctrlHdl 0x%0lx, ctrlBase 0x%0lx, wndHdl 0x%0lx,"
    		" wndBase 0x%0lx, size 0x%0lx\n",
    		pDrvCtrl->cntlHandle, pDrvCtrl->cntlBase, pDrvCtrl->windHandle,
			pDrvCtrl->windBase, pDrvCtrl->windSize, 6);

    pDrvCtrl->curWindBase = 0;

    pFdtDev = vxbFdtDevGet(pDev);
    if (pFdtDev == NULL)
        goto error;

    /* announce all  devices connected on bus */

    offset = pFdtDev->offset;

    for (offset = VX_FDT_CHILD(offset); offset > 0;
         offset = VX_FDT_PEER(offset))
        {
        pCur = NULL;

        if (vxbDevCreate (&pCur) != OK)
            {
            continue;
            }

        pAddrExtenderDevInfo = (ADDR_EXTENDER_DEV_INFO *)vxbMemAlloc \
                                    (sizeof(*pAddrExtenderDevInfo));

        if (pAddrExtenderDevInfo == NULL)
            {
            (void) vxbDevDestroy(pCur);
            continue;
            }

        pNewFdtDev = &pAddrExtenderDevInfo->vxbFdtDev;

         /* Get the device basic infomation  */

        vxbFdtDevSetup(offset, pNewFdtDev);
        vxbDevNameSet(pCur, pNewFdtDev->name, FALSE);

        if (vxbResourceInit(&pAddrExtenderDevInfo->vxbResList) != OK)
            {
            (void) vxbDevDestroy(pCur);
            vxbMemFree (pAddrExtenderDevInfo);
            continue;
            }

        if (vxbFdtRegGet(&pAddrExtenderDevInfo->vxbResList, pNewFdtDev) != OK)
            {
            vxbFdtResFree(&pAddrExtenderDevInfo->vxbResList);
            vxbMemFree(pAddrExtenderDevInfo);
            (void) vxbDevDestroy(pCur);
            continue;
            }

        /* Assign the bus internal variable and type  */

        vxbDevIvarsSet(pCur, (void *)pAddrExtenderDevInfo);
        vxbDevClassSet(pCur, VXB_BUSID_FDT);

        (void) vxbDevAdd(pDev, pCur);
        }

    return OK;
error:
    if (pRes0 != NULL)
        (void)vxbResourceFree (pDev, pRes0);
    if (pRes1 != NULL)
        (void)vxbResourceFree (pDev, pRes1);
    vxbMemFree ((char *)pDrvCtrl);

    return ERROR;
    }


/*******************************************************************************
*
* addrExtenderAccess - IO access via Address Extender
*
* This routine allows IO access to remote resources via SDIO
*
* RETURNS: 0 in case of write access or read access return value.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT32 addrExtenderAccess
    (
    UINT32 handle,
    UINT32 offset,
    UINT32 value
    )
    {
    UINT32              data = 0;
    ADDR_EXTENDER_CTRL  *pDrvCtrl = gAddrExtenderDrvCtrl;
    UINT32              acceswidth = VXB_HANDLE_WIDTH(handle);
    UINT32              windSize = pDrvCtrl->windSize;
    UINT32              slaveOffset;

    /* Unaligned accesses are not allowed */
    if ((acceswidth <= 0) || (acceswidth == 3) ||  (acceswidth > 4) || (offset & (acceswidth - 1))) {
        ADDR_EXTENDER_DBG(ADDR_EXTENDER_DBG_ERR,
        		"addrExtenderAccess: unaligned access @ %0lx with width %d\n",
				offset, acceswidth, 3, 4, 5, 6);
        return 0;
    }

    if (offset < pDrvCtrl->curWindBase ||
    		offset >= pDrvCtrl->curWindBase + windSize) {

    	/* Update the current base of the window_slave */
    	pDrvCtrl->curWindBase = offset & ~(windSize - 1);

    	vxbWrite32 (pDrvCtrl->cntlHandle, (UINT32 *)pDrvCtrl->cntlBase,
    			pDrvCtrl->curWindBase);
    }

    slaveOffset = offset - pDrvCtrl->curWindBase;

    if (VXB_HANDLE_OP(handle) == VXB_HANDLE_OP_READ)
        {

        /* read access */

        switch (acceswidth)
            {
            case 1:
                data = (UINT32) vxbRead8 (pDrvCtrl->windHandle,
                        (UINT8 *)(pDrvCtrl->windBase + slaveOffset));
                break;
            case 2:
                data = (UINT32) vxbRead16 (pDrvCtrl->windHandle,
                        (UINT16 *)(pDrvCtrl->windBase + slaveOffset));
                break;
            case 4:
            default:
                data = vxbRead32 (pDrvCtrl->windHandle,
                        (UINT32 *)(pDrvCtrl->windBase + slaveOffset));
                break;
            }
        }
    else /* VXB_HANDLE_OP_WRITE */
        {

        /* write access */

        switch (acceswidth)
            {
            case 1:
                vxbWrite8 (pDrvCtrl->windHandle, (UINT8 *)(pDrvCtrl->windBase +
                    slaveOffset), value & 0xff);
                break;
            case 2:
                vxbWrite16 (pDrvCtrl->windHandle, (UINT16 *)(pDrvCtrl->windBase
                    + slaveOffset), value & 0xffff);
                break;
            case 4:
            default:
                vxbWrite32 (pDrvCtrl->windHandle, (UINT32 *)(pDrvCtrl->windBase
                    + slaveOffset), value);
                break;
            }
        }

    return data;
    }

/*******************************************************************************
*
* addrExtenderResAlloc - allocate resource for child device
*
* This routine allocates resources for child devices and go corresponding
* operation based on its type.
*
* RETURNS: pointer to allocated resource
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VXB_RESOURCE * addrExtenderResAlloc
    (
    VXB_DEV_ID pDev,
    VXB_DEV_ID pChild,
    UINT32     id
    )
    {
    ADDR_EXTENDER_DEV_INFO  *pAddrExtenderDevInfo;
    VXB_RESOURCE            *pVxbRes;
    VXB_RESOURCE_ADR        *pVxbAdrRes;
    ADDR_EXTENDER_CTRL      *pDrvCtrl;

    pDrvCtrl = vxbDevSoftcGet(pDev);
    pAddrExtenderDevInfo = (ADDR_EXTENDER_DEV_INFO *) vxbDevIvarsGet (pChild);

    if (pAddrExtenderDevInfo == NULL)
        {
        return NULL;
        }

    pVxbRes = vxbResourceFind (&pAddrExtenderDevInfo->vxbResList, id);

    if (pVxbRes == NULL)
        {
        return NULL;
        }

    if (VXB_RES_TYPE (pVxbRes->id) == VXB_RES_MEMORY)
        {
        pVxbAdrRes = pVxbRes->pRes;

        if (pVxbAdrRes->size == 0)
            {
            ADDR_EXTENDER_DBG(ADDR_EXTENDER_DBG_ERR, "addrExtenderResAlloc() "
                "- resource size is zero\n", 0, 0, 0, 0, 0, 0);
            return NULL;
            }

        pVxbAdrRes->virtual = (VIRT_ADDR) pVxbAdrRes->start;
        pVxbAdrRes->pHandle = (void *)addrExtenderAccess;

        /* workaround for initializing the EIM bus */
        pDrvCtrl->curWindBase = (UINT32)(pVxbAdrRes->start & ~(pDrvCtrl->windSize - 1));
    	vxbWrite32 (pDrvCtrl->cntlHandle, (UINT32 *)pDrvCtrl->cntlBase,
    			pDrvCtrl->curWindBase);


        return pVxbRes;
        }
    else
        return NULL;

    }


/*******************************************************************************
*
* addrExtenderResFree - free the resource for child device
*
* This routine free the resource allocated by calling addrExtenderResAlloc
*
* RETURNS: OK if free successfully, otherwise ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS addrExtenderResFree
    (
    VXB_DEV_ID      pDev,
    VXB_DEV_ID      pChild,
    VXB_RESOURCE    *pRes
    )
    {
    ADDR_EXTENDER_DEV_INFO * pAddrExtenderDevInfo;

    pAddrExtenderDevInfo = (ADDR_EXTENDER_DEV_INFO *)vxbDevIvarsGet(pChild);

    if (pAddrExtenderDevInfo == NULL)
        return ERROR;

    if (VXB_RES_TYPE(pRes->id) != VXB_RES_MEMORY)
        return ERROR;

    return OK;
    }


/*******************************************************************************
*
* addrExtenderResListGet - get the resource list of specific device
*
* This routine get the resource list of specific device
*
* RETURNS: resource list pointer
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VXB_RESOURCE_LIST * addrExtenderResListGet
    (
    VXB_DEV_ID pDev,
    VXB_DEV_ID pChild
    )
    {
    ADDR_EXTENDER_DEV_INFO * pAddrExtenderDevInfo;

    pAddrExtenderDevInfo = (ADDR_EXTENDER_DEV_INFO *) vxbDevIvarsGet (pChild);

    if (pAddrExtenderDevInfo == NULL)
        {
        return NULL;
        }

    return &pAddrExtenderDevInfo->vxbResList;
    }


/*******************************************************************************
*
* addrExtenderDevInfo - get the child device information
*
* This routine gets the  child device information
*
* RETURNS: the device information pointer
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VXB_FDT_DEV * addrExtenderDevInfo
    (
    VXB_DEV_ID pDev,
    VXB_DEV_ID pChild
    )
    {
    ADDR_EXTENDER_DEV_INFO * pAddrExtenderDevInfo;

    if (pChild == NULL)
        {
        return NULL;
        }

    pAddrExtenderDevInfo = vxbDevIvarsGet (pChild);

    if (pAddrExtenderDevInfo == NULL)
        {
        return NULL;
        }

    return &pAddrExtenderDevInfo->vxbFdtDev;
    }
