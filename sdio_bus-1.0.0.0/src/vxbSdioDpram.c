/* vxbSdioDpram.c - SDIO DPRAM device driver file */

/*
 * Copyright (c) 2015, 2016 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
25feb16,hni  fixed PC-lint warnings (GER-DRA12980)
24feb16,cis  code review modifications (GER-DRA12980 TRACE A0050)
18feb16,hni  Merged changes in test function, needed for running
             GER-DRA12980 ATP.
14feb16,cis  Fix for TRACE GER-DRA12980 PR0030
10jul20,vpo  written
*/

/*
DESCRIPTION
This module implements a driver for the DPRAM attached to the SDIO bus.

\ie
SEE ALSO: vxBus
*/

/* includes */

#include <vxWorks.h>
#include <string.h>
#include <stdio.h>

#include <hwif/vxBus.h>
#include <hwif/buslib/vxbFdtLib.h>

#include <vxbSdMmcLib.h>
#include <vxbSdio.h>
#include <hwif/methods/vxbSdioMethod.h>
#include <m48RawIpcBase.h>
#include <vxbSdioDpram.h>

#undef SDIO_DPRAM_TEST

/*Enable debug info*/
#undef SDIO_DPRAM_DBG_ON
#ifdef  SDIO_DPRAM_DBG_ON

#include <private/kwriteLibP.h>         /* _func_kprintf */
#ifdef  LOCAL
#undef  LOCAL
#define LOCAL
#endif

#define SDIO_DPRAM_DBG_INIT           0x00000001
#define SDIO_DPRAM_DBG_RW             0x00000002
#define SDIO_DPRAM_DBG_ERR            0x00000004
#define SDIO_DPRAM_DBG_INFO           0x00000008
#define SDIO_DPRAM_DBG_ALL            0xffffffff
#define SDIO_DPRAM_DBG_OFF            0x00000000

LOCAL UINT32 sdioDpramDbgMask = SDIO_DPRAM_DBG_ERR;

#define SDIO_DPRAM_DBG(mask, string, a, b, c, d, e, f)           \
    if ((sdioDpramDbgMask & mask) || (mask == SDIO_DPRAM_DBG_ALL))  \
        if (_func_kprintf != NULL) \
           (* _func_kprintf)(string, a, b, c, d, e, f)

#else
#   define SDIO_DPRAM_DBG(mask, string, a, b, c, d, e, f)
#endif  /* SDIO_DPRAM_DBG_ON */


LOCAL STATUS sdioDpramProbe (VXB_DEV_HANDLE);
LOCAL STATUS sdioDpramAttach (VXB_DEV_HANDLE);


/* Suppress clang-tidy message. This initialization action is provided by VxWorks. We cannot change it. */
/* NOLINTNEXTLINE(cppcoreguidelines-interfaces-global-init) */
LOCAL VXB_DRV_METHOD vxbSdioDpram_methods[] =
    {
    { VXB_DEVMETHOD_CALL(vxbDevProbe), sdioDpramProbe },
    { VXB_DEVMETHOD_CALL(vxbDevAttach), sdioDpramAttach },

    VXB_DEVMETHOD_END
    };

LOCAL VXB_FDT_DEV_MATCH_ENTRY fdtSdioDpramMatch[] =
    {
        {
        "alt,avalon_mem",               /* compatible */
        NULL                            /* no configuration */
        },
        {}                              /* empty terminated list */
    };

/* SDIO DPRAM openfirmware driver */

VXB_DRV vxbSdioDpramDrv =
    {
    { NULL } ,
    SDIO_DPRAM_NAME,      /* Name */
    "SDIO DPRAM driver",  /* Description */
    VXB_BUSID_SDIO,       /* Class */
    0,                    /* Flags */
    0,                    /* Reference count */
    vxbSdioDpram_methods  /* Method table */
    };

VXB_DRV_DEF(vxbSdioDpramDrv)


LOCAL  VXB_DEV_HANDLE g_vxbDpramDevs[MAX_IPC_UNITS] = {0};

/*******************************************************************************
*
* sdioDpramProbe - vxbus probe function
*
* This function is called by vxBus to probe device.
*
* RETURNS: OK if detected device is an SDIO DPRAM device. ERROR otherwise.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioDpramProbe
    (
    VXB_DEV_HANDLE pDev
    )
    {
    STATUS rc;

    rc = vxbFdtDevMatch (pDev, fdtSdioDpramMatch, NULL);

    return rc;
    }

/*******************************************************************************
*
* sdioDpramAttach - VxBus attach
*
* RETURNS: OK or ERROR when initialization failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioDpramAttach
    (
    VXB_DEV_HANDLE pDev
    )
    {
    VXB_FDT_DEV         *pFdtDev;
    VXB_RESOURCE        *pRes  = NULL;
    SDIO_DPRAM_CTRL     *pDrvCtrl;
    const void          *pValue;
    int                 len;
    int                 unit;

    pFdtDev = vxbFdtDevGet(pDev);
    if (pFdtDev == NULL)
        return ERROR;

    pRes = vxbResourceAlloc (pDev, VXB_RES_MEMORY, 0);
    if ((pRes == NULL) || (pRes->pRes == NULL))
       return ERROR;

    pDrvCtrl = (SDIO_DPRAM_CTRL *) vxbMemAlloc (sizeof (SDIO_DPRAM_CTRL));
    if (pDrvCtrl == NULL)
        goto error;
    vxbDevSoftcSet(pDev, pDrvCtrl);
    pDrvCtrl->pInst = pDev;

    pDrvCtrl->ramHandle = ((VXB_RESOURCE_ADR *) (pRes->pRes))->pHandle;
    pDrvCtrl->ramBase   = (UINT32)((VXB_RESOURCE_ADR *)(pRes->pRes))->virtual;
    pDrvCtrl->ramSize   = (UINT32)((VXB_RESOURCE_ADR *)(pRes->pRes))->size;

    pValue = vxFdtPropGet(pFdtDev->offset, "unit", &len);
    if (pValue == NULL)
        goto error;
    unit = vxFdt32ToCpu(*(UINT32 *)pValue);

    if (unit >= MAX_IPC_UNITS)
        {
        SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_ERR, "DPRAM unit %d too big\n", unit,
            0, 0, 0, 0, 0);
        goto error;
        }
    pDrvCtrl->unit = unit;

    if (g_vxbDpramDevs[unit])
        {
        SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_ERR, "DPRAM unit %d already attached\n",
            unit, 0, 0, 0, 0, 0);
        goto error;
        }

    g_vxbDpramDevs[unit] = pDev;

    SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_INIT,
        "vxbAttach OK: unit %d ramBase=0x%x, ramSize=0x%x, ramHandle=0x%x\n",
        pDrvCtrl->unit, pDrvCtrl->ramBase, pDrvCtrl->ramSize,
        pDrvCtrl->ramHandle, 0, 0);

    return OK;

error:
    if (pRes != NULL)
        (void)vxbResourceFree (pDev, pRes);
    vxbMemFree ((char *)pDrvCtrl);

    return ERROR;
    }

/*******************************************************************************
*
* dpramRead - Read function
*
* This function reads an area of one of the DPRAM memories.
*
* RETURNS: OK if read succeeded ERROR if the parmeter checks or SDIO transfer
*          failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS dpramRead
    (
    int     unit,
    UINT32  dpramAddr,
    UINT8   *pBuffer,
    UINT16  len
    )
    {
    VXB_DEV_HANDLE      pDev;
    VXB_DEV_HANDLE      pParent;
    SDIO_DPRAM_CTRL     *pDrvCtrl;

    if ((unit >= MAX_IPC_UNITS) || (unit < 0) || (pBuffer == NULL))
        return ERROR;

    pDev = g_vxbDpramDevs[unit];
    if (pDev == NULL)
        {
        SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_ERR, "DPRAM Unit %d not attached\n",
            unit, 0, 0, 0, 0, 0);
        return ERROR;
        }

    pParent = vxbDevParent(pDev);

    if (pParent == NULL) {
        SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_ERR, "DPRAM Unit %d no pParent\n",
            unit,0,0,0,0,0);
        return ERROR;
    }

    pDrvCtrl = vxbDevSoftcGet(pDev);
    if (pDrvCtrl == NULL) {
        SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_ERR, "DPRAM Unit %d parameter ERROR\n",
            unit,0,0,0,0,0);
        return ERROR;
    }

    if ((dpramAddr < pDrvCtrl->ramBase) ||
        (dpramAddr + len > pDrvCtrl->ramBase + pDrvCtrl->ramSize))
        {
        SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_ERR, "DPRAM area [0x%x, 0x%x] doesn't"
            " fit into configured DPRAM\n", dpramAddr, len, 0, 0, 0, 0);
        return ERROR;
        }

    return  VXB_SDIO_READ_BYTES (pParent, pBuffer, len, dpramAddr);
    }

/*******************************************************************************
*
* dpramWrite - Write function
*
* This function writes an area of one of the DPRAM memories.
*
* RETURNS: OK if read succeeded ERROR if the parmeter checks or SDIO transfer
*          failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS dpramWrite
    (
    int     unit,
    UINT32  dpramAddr,
    UINT8   *pBuffer,
    UINT16  len
    )
    {
    VXB_DEV_HANDLE      pDev;
    VXB_DEV_HANDLE      pParent;
    SDIO_DPRAM_CTRL     *pDrvCtrl;

    if ((unit >= MAX_IPC_UNITS) || (unit < 0) || (pBuffer == NULL))
        return ERROR;

    pDev = g_vxbDpramDevs[unit];
    if (pDev == NULL)
        {
        SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_ERR, "DPRAM Unit %d not attached\n",
            unit, 0, 0, 0, 0, 0);
        return ERROR;
        }

    pParent = vxbDevParent(pDev);

    if (pParent == NULL)
        return ERROR;

    pDrvCtrl = vxbDevSoftcGet(pDev);
    if (pDrvCtrl == NULL)
        return ERROR;

    if ((dpramAddr < pDrvCtrl->ramBase) ||
        (dpramAddr + len > pDrvCtrl->ramBase + pDrvCtrl->ramSize))
        {
        SDIO_DPRAM_DBG (SDIO_DPRAM_DBG_ERR, "DPRAM area [0x%x, 0x%x] doesn't"
            "fit into configured DPRAM\n", dpramAddr, len, 0, 0, 0, 0);
        return ERROR;
        }

    return VXB_SDIO_WRITE_BYTES (pParent, pBuffer, len, dpramAddr);
    }

#ifdef SDIO_DPRAM_TEST
#define TEST_BUFFER_SIZE 512

UINT8 * pWriteData = NULL;
UINT8 * pReadData = NULL;

/*******************************************************************************
*
* testDpram - Dpram test using blocks of data
*
* This function executes a test of the driver's read an write functions using
* blocks of data. The test does a write - read - compare sequence using the
* testData parameter value.
*
* RETURNS: OK if the test succeeds, ERROR if the read, write or compare
*          operations failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS testDpram
    (
    int    unit,
    UINT32 dpramAddr,
    UINT8  testData,
    UINT16 blockSize
    )
    {
    STATUS rc = OK;
    UINT16 i;

    printf ("Testing DPRAM with block size of %d\n", blockSize);
    if ((pWriteData = (UINT8 *) malloc (blockSize)) == NULL)
        {
        printf ("testDpram: pWriteData malloc failed.\n");
        return (ERROR);
        }
    memset(pWriteData, testData, blockSize);

    if ((pReadData = (UINT8 *) malloc (blockSize)) == NULL)
        {
        printf ("testDpram: pReadData malloc failed.\n");
        return (ERROR);
        }

    rc = dpramWrite(unit, dpramAddr, pWriteData, blockSize);
    if (rc == ERROR)
        {
        printf ("testDpram: write %d bytes at dpram address 0x%x failed!\n",
                blockSize, dpramAddr);
        return rc;
        }

    memset(pReadData, 0x00, blockSize);

    rc = dpramRead(unit, dpramAddr, pReadData,blockSize);
    if (rc == ERROR)
    {
        printf ("testDpram: read %d bytes at dpram address 0x%x failed!\n",
                blockSize, dpramAddr);
        return rc;
    }

    for (i = 0; i < blockSize; i++)
    {
        if (pWriteData[i] != pReadData[i])
        {
            printf ("testDpram: check failed at byte_no %d, writtenData:"
                " 0x%02x, pReadData:0x%02x\n", i, pWriteData[i], pReadData[i]);
            rc = ERROR;
        }
    }

    if (rc == OK)
        printf ("testDpram SUCCESS!\n");
    else
        printf ("testDpram FAILED!\n");

    return rc;
    }

/*******************************************************************************
*
* testDpram0 - Dpram test using vxbWrite and vxbRead functions
*
* This function executes a test of the driver's read an write functions through
* the VxWorks read and write standard functions vxbWrite and vxbRead. The test
* does a write - read - compare sequence using incremental values.
*
* RETURNS: OK if the test succeeds, ERROR if the read, write or compare
*          operations failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS testDpram0
    (
    int    unit,
    UINT32 dpramAddr,
    UINT16 count,
    int    width
    )
    {
    VXB_DEV_HANDLE      pDev;
    SDIO_DPRAM_CTRL     *pDrvCtrl;
    STATUS              rc = OK;
    UINT32              addr = dpramAddr;
    UINT16              i = 0;
    UINT16              readCounter;


    pDev = g_vxbDpramDevs[unit];
    if (pDev == NULL)
        {
        printf ("DPRAM Unit %d not attached\n", unit);
        return ERROR;
        }

    pDrvCtrl = vxbDevSoftcGet(pDev);
    if (pDrvCtrl == NULL)
        return ERROR;

    if ((dpramAddr < pDrvCtrl->ramBase) ||
        (dpramAddr + count*width > pDrvCtrl->ramBase + pDrvCtrl->ramSize))
        {
        printf ("DPRAM area [0x%x, 0x%x] doesn't fit into configured DPRAM\n",
            dpramAddr, count*width);
        return ERROR;
        }

    do
        {
        switch (width)
            {
            case 1:
                vxbWrite8(pDrvCtrl->ramHandle, (UINT8 *)addr,
                    (UINT8)(i & 0xff) );
                break;

            case 2:
                vxbWrite16(pDrvCtrl->ramHandle, (UINT16 *)addr, i);
                break;

            case 4:
                vxbWrite32(pDrvCtrl->ramHandle, (UINT32 *)addr, (UINT32)i);
                break;

            default:
                {
                printf ("Incorrect width %d. Must be either 1, 2 or 4\n",
                    width);
                return ERROR;
                }
            }

        i++;
        addr += width;
        }
    while (i < count);

    addr = dpramAddr;
    i = 0;

    do
        {
        UINT16 expected_counter = i;

        switch (width)
            {
            case 1:
                {
                UINT8 byteCounter;
                byteCounter = vxbRead8(pDrvCtrl->ramHandle, (UINT8 *)addr);
                readCounter = (UINT16)byteCounter;
                expected_counter = (UINT16) (i & 0xff);
                break;
                }

            case 2:
                readCounter = vxbRead16(pDrvCtrl->ramHandle, (UINT16 *)addr);
                break;

            case 4:
                readCounter = (UINT16) vxbRead32(pDrvCtrl->ramHandle,
                    (UINT32 *)addr);
                break;

            default:
                {
                printf ("Incorrect width %d. Must be either 1, 2 or 4\n",
                    width);
                return ERROR;
                }
            }

        if (readCounter != expected_counter)
            {
            printf ("testDpram0: check failed at SDIO offset 0x%x, writtenData:"
            "0x%x, readData:0x%x\n", addr, expected_counter, readCounter);
            rc = ERROR;
            }

        i++;
        addr += width;
        }
    while (i < count);

    if (rc == OK)
        printf ("testDpram SUCCESS!\n");
    else
        printf ("testDpram FAILED!\n");

    return rc;
    }

#endif

BOOL isIpcBufferInitialized(int channel) {
	if (channel < MAX_IPC_UNITS)
		return (g_vxbDpramDevs[channel] != NULL);
	else
		return FALSE;
}

STATUS writeIpcBuffer(int channel, unsigned int offset, void* buffer, unsigned int length) {

    return dpramWrite(channel, offset, buffer, length);

}

STATUS readIpcBuffer (int channel, unsigned int offset, void* buffer, unsigned int length) {

    return dpramRead(channel, offset, buffer, length);
}

UINT32 getIpcBufferSize (int channel) {
    VXB_DEV_HANDLE pDev;
    SDIO_DPRAM_CTRL *pDrvCtrl;

    if (channel >= MAX_IPC_UNITS)
    {
        printf ("DPRAM invalid unit id %d\n", channel);
        return 0;
    }

    pDev = g_vxbDpramDevs[channel];
    if (pDev == NULL)
    {
        printf ("DPRAM Unit %d not attached\n", channel);
        return 0;
    }

    pDrvCtrl = vxbDevSoftcGet(pDev);
    if (pDrvCtrl == NULL) {
        return 0;
    }

    return pDrvCtrl->ramSize;
}
