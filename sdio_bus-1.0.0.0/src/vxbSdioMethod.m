#include <vxWorks.h>
#include <hwif/vxBus.h>

METHOD STATUS vxbSdioReadBytes(VXB_DEV_ID pDev, UINT8 * buffer, UINT16 length, UINT32 offset); default[ERROR] # "Read multi bytes from SDIO device to host"
METHOD STATUS vxbSdioWriteBytes(VXB_DEV_ID pDev, const UINT8 * buffer, UINT16 length, UINT32 offset); default[ERROR] # "Write multi bytes to SDIO device by host"
