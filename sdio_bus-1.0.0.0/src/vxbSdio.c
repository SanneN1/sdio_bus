/* vxbSdio.c - SDIO device driver file */

/*
 * Copyright (c) 2015, 2016 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
23feb16,cis  code review modifications (GER-DRA12980 TRACE A0049)
05nov15,cis  Added fix for trace GER-DRA12980 B0004 SDIO bus collisions
10jul15,vpo  written
*/

/*
DESCRIPTION
This module implements a driver for SDIO bus.
Currently the driver doesn't have a proper "probe" routine; it attaches
to any SDIO function device detected during SD Bus enumeration.

The driver supports only a limited number of instantiations
    #define MAX_SDIO_BUSES      2
because of the mechanism used to provide access to registers on the SDIO bus.
More exactly, children devices uses vxbReadn/vxbWriten routines with a
special function as resource handle (see sdioResAlloc() ). For each
instantiation of the SDIO bus driver, we need a different acces function as
handle (e.g. accessSdio_0() and accessSdio_1()). These behaves as trampolines to
the generic function that provides access to SDIO registers(accessSdioOffset()).


To add the driver to your vxWorks image, add the following component to the
kernel configuration.

/cs
vxprj component add  DRV_SDIO_BUS
/ce

\ie
SEE ALSO: vxBus
*/

/* includes */

#include <vxWorks.h>
#include <string.h>

#include <hwif/vxBus.h>
#include <hwif/buslib/vxbFdtLib.h>

#include <vxbSdMmcLib.h>
#include <vxbSdio.h>
#include <hwif/methods/vxbSdioMethod.h>
#include <semLib.h>     /* For semaphores */

#undef SDIODBG_ON
#ifdef  SDIODBG_ON

#include <private/kwriteLibP.h>         /* _func_kprintf */
#ifdef  LOCAL
#undef  LOCAL
#define LOCAL
#endif

#define SDIO_DBG_INIT           0x00000001
#define SDIO_DBG_RW             0x00000002
#define SDIO_DBG_ERR            0x00000004
#define SDIO_DBG_INFO           0x00000008
#define SDIO_DBG_ALL            0xffffffff
#define SDIO_DBG_OFF            0x00000000

LOCAL UINT32 sdioDbgMask = SDIO_DBG_ALL;

#define SDIO_DBG(mask, string, a, b, c, d, e, f)           \
    if ((sdioDbgMask & mask) || (mask == SDIO_DBG_ALL))  \
        if (_func_kprintf != NULL) \
           (* _func_kprintf)(string, a, b, c, d, e, f)
#else
#   define SDIO_DBG(mask, string, a, b, c, d, e, f)
#endif  /* SDIO_DBG_ON */

#define PRINT_CCCR_VALUE(name) SDIO_DBG(SDIO_DBG_INFO, "sdioReadCccr: %s: \
    0x%02x\n", (_Vx_usr_arg_t)#name, pSdioInfo->cccr.name, 0, 0, 0, 0)

#if (_BYTE_ORDER == _BIG_ENDIAN)
# define CPU_TO_LE16(x)     vxbSwap16(x)
# define CPU_TO_LE32(x)     vxbSwap32(x)
# define LE_TO_CPU16(x)     vxbSwap16(x)
# define LE_TO_CPU32(x)     vxbSwap32(x)
#elif (_BYTE_ORDER == _LITTLE_ENDIAN)
# define CPU_TO_LE16(x)     (x)
# define CPU_TO_LE32(x)     (x)
# define LE_TO_CPU16(x)     (x)
# define LE_TO_CPU32(x)     (x)
#else
#error Undef _BYTE_ORDER ?
#endif

#undef SDIO_NO_HIGHSPEED

BUSTYPE_DEF(vxbSdioBus, "SDIO bus type")


LOCAL STATUS sdioDevProbe (VXB_DEV_HANDLE);
LOCAL STATUS sdioAttach (VXB_DEV_HANDLE);
LOCAL STATUS sdioReadMultiBytesFromDevice (VXB_DEV_HANDLE, UINT8 *,\
                UINT16, UINT32);
LOCAL STATUS sdioWriteMultiBytesToDevice (VXB_DEV_HANDLE, UINT8 *, UINT16,\
                UINT32);
LOCAL VXB_RESOURCE * sdioResAlloc (VXB_DEV_HANDLE, VXB_DEV_HANDLE, UINT32);
LOCAL STATUS sdioResFree (VXB_DEV_HANDLE, VXB_DEV_HANDLE, VXB_RESOURCE * pRes);
LOCAL VXB_RESOURCE_LIST * sdioResListGet(VXB_DEV_HANDLE, VXB_DEV_HANDLE);
LOCAL VXB_FDT_DEV * sdioDevInfo (VXB_DEV_HANDLE, VXB_DEV_HANDLE);

LOCAL STATUS sdioBusInfoGet (VXB_DEV_HANDLE);
LOCAL STATUS sdioBusConfig (VXB_DEV_HANDLE);
LOCAL STATUS sdioCisDataGet (SD_HARDWARE *,    UINT8, UINT8, UINT8, UINT8 *);
LOCAL STATUS sdioSetBlockSize (VXB_DEV_HANDLE, UINT16);
LOCAL STATUS sdioReadCccr (VXB_DEV_HANDLE);
LOCAL UINT32 accessSdio_0 (UINT32, UINT32, UINT32);
LOCAL UINT32 accessSdio_1 (UINT32, UINT32, UINT32);
LOCAL UINT32 accessSdioOffset (UINT32, UINT32, UINT32, VXB_DEV_HANDLE);
LOCAL STATUS sdioIdxAlloc (SDIO_CTRL *);
LOCAL STATUS sdioTransferBytes (VXB_DEV_HANDLE, UINT8 *, UINT16, UINT32, BOOL);
LOCAL STATUS sdioReadByte (VXB_DEV_HANDLE, UINT32, UINT8 *);
LOCAL STATUS sdioReadWord (VXB_DEV_HANDLE, UINT32, UINT16 *);
LOCAL STATUS sdioReadLong (VXB_DEV_HANDLE, UINT32, UINT32 *);
LOCAL STATUS sdioWriteByte (VXB_DEV_HANDLE, UINT32, UINT8);
LOCAL STATUS sdioWriteWord (VXB_DEV_HANDLE, UINT32, UINT16);
LOCAL STATUS sdioWriteLong (VXB_DEV_HANDLE, UINT32, UINT32);

/* Suppress clang-tidy message. This initialization action is provided by VxWorks. We cannot change it. */
/* NOLINTNEXTLINE(cppcoreguidelines-interfaces-global-init) */
LOCAL VXB_DRV_METHOD vxbSdio_methods[] =
    {
    { VXB_DEVMETHOD_CALL(vxbDevProbe), sdioDevProbe },
    { VXB_DEVMETHOD_CALL(vxbDevAttach), sdioAttach },

    { VXB_DEVMETHOD_CALL(vxbSdioReadBytes),\
        (FUNCPTR)sdioReadMultiBytesFromDevice},
    { VXB_DEVMETHOD_CALL(vxbSdioWriteBytes),\
        (FUNCPTR)sdioWriteMultiBytesToDevice},

    { VXB_DEVMETHOD_CALL (vxbResourceAlloc),   (FUNCPTR)sdioResAlloc },
    { VXB_DEVMETHOD_CALL (vxbResourceFree),    (FUNCPTR)sdioResFree },
    { VXB_DEVMETHOD_CALL (vxbResourceListGet), (FUNCPTR)sdioResListGet },
    { VXB_DEVMETHOD_CALL (vxbFdtDevGet),       (FUNCPTR)sdioDevInfo },

    VXB_DEVMETHOD_END
    };


/* SDIO openfirmware driver */

VXB_DRV vxbSdioDrv =
    {
    { NULL } ,
    SDIO_NAME,         /* Name */
    "SDIO driver",     /* Description */
    VXB_BUSID_SDMMC,      /* Class */
    0,                    /* Flags */
    0,                    /* Reference count */
    vxbSdio_methods  /* Method table */
    };

VXB_DRV_DEF(vxbSdioDrv)

/* SDIO global bus index */

LOCAL UINT32 sdioDevIdx;

VXB_SDIO_ACCESS vxbSdioAccess[MAX_SDIO_BUSES] = {{(FUNCPTR)accessSdio_0,0},
                                                 {(FUNCPTR)accessSdio_1, 0}};


/*******************************************************************************
*
* sdioDevProbe - vxbus probe function
*
* This function is called by vxBus to probe device.
*
* RETURNS: TRUE if the device is an SDIO device. FALSE otherwise.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioDevProbe
    (
    VXB_DEV_HANDLE pDev
    )
    {
    SD_HARDWARE * pSdHardWare;


    pSdHardWare = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardWare == NULL)
        return ERROR;

    if (! pSdHardWare->isSdio)
        return ERROR;

    vxbDevNameSet (pDev, SDIO_NAME, FALSE);

    return TRUE;
    }

/*******************************************************************************
*
* sdioAttach - VxBus attach function
*
* This is the SDIO Bus initialization routine. It configures the
* SDIO function (bus width, speed, max block size) based on the
* max values advertised by the SDIO function itself.
* Moreover, it enumerates all child devices on the SDIO bus and
* creates vxBus devices in the vxBus hierarchy.
* (The child devices are configured in the FDT).
*
* RETURNS: OK or ERROR in case of initialization failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioAttach
    (
    VXB_DEV_HANDLE pDev
    )
    {
    SDIO_CTRL      *pDrvCtrl;
    SD_HARDWARE    *pSdHardware;
    VXB_DEV_ID      pCur;
    SDIO_DEV_INFO  *pSdioDevInfo;
    VXB_FDT_DEV    *pFdtDev;
    int             offset;
    int             parOffset;

    pSdHardware = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardware == NULL)
        {
        SDIO_DBG (SDIO_DBG_ERR, "sdioAttach() - pSdHardWare is NULL\n",
                    0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    /*Allocate SDIO control structure*/

    pDrvCtrl = (SDIO_CTRL *) vxbMemAlloc (sizeof (SDIO_CTRL));
    if (pDrvCtrl == NULL)
        {
        SDIO_DBG(SDIO_DBG_ERR, "sdioAttach() -  malloc SDIO_CTRL fault\n",
            0, 0, 0, 0, 0, 0);
        return ERROR;
        }
    vxbDevSoftcSet (pDev, pDrvCtrl);
    pDrvCtrl->pInst = pDev;

    /* apply one card index to system */

    if (sdioIdxAlloc (pDrvCtrl) == ERROR)
        {
        SDIO_DBG(SDIO_DBG_ERR, "sdioBusInstInit() -  sdioIdxAlloc fault\n",
            0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    if (sdioBusInfoGet (pDev) == ERROR)
        {
        SDIO_DBG(SDIO_DBG_ERR, "sdioAttach() -  sdioBusInfoGet fault\n",
            0, 0, 0, 0, 0, 0);
        goto Exit;
        }

#ifdef SDIO_NO_HIGHSPEED
        pDrvCtrl->highSpeed = FALSE;
#else
        pDrvCtrl->highSpeed = TRUE;
#endif

    if (sdioBusConfig (pDev) == ERROR)
        {
        SDIO_DBG(SDIO_DBG_ERR, "sdioAttach() -  sdioBusConfig fault\n",
            0, 0, 0, 0, 0, 0);
        goto Exit;
        }

    if ( ! pSdHardware->isSpi) { /* This is Draeger fixed,reduced SDIO */

    	/*Enable sdio function */
    	if (vxbSdioSetFunc (pDev, (UINT8)(pSdHardware->funcNum), TRUE) == ERROR)
    	{
    		SDIO_DBG(SDIO_DBG_ERR, "sdioBusInstInit() - vxbSdioSetFunc() NOK\n",
    				0, 0, 0, 0, 0, 0);
    		goto Exit;
    	}
    }

    pDrvCtrl->sdioAccessSem = semMCreate (SEM_Q_PRIORITY | SEM_DELETE_SAFE |
                                      SEM_INVERSION_SAFE);
    if (!pDrvCtrl->sdioAccessSem)
        {
        SDIO_DBG(SDIO_DBG_ERR, "sdioBusInstInit() - could not allocate "
        "semaphore sdioAccessSem\n", 0, 0, 0, 0, 0, 0);
        goto Exit;
        }

    vxbSdioAccess[pDrvCtrl->idx].pdev = pDev;

    pDrvCtrl->pParentFdtDev = vxbFdtDevGet (pSdHardware->pHostDev);

    for (offset = vxFdtNodeOffsetByCompatible (0, "sdio_bus");
         offset > 0;
         offset = vxFdtNodeOffsetByCompatible (offset, "sdio_bus"))
        {
        parOffset = vxFdtParentOffset (offset);
        if (parOffset == pDrvCtrl->pParentFdtDev->offset)
            break;
        }

    if (offset < 0)
        {
        SDIO_DBG (SDIO_DBG_ERR, "sdioAttach() - No parent discovered in DTS\n",
                    0, 0, 0, 0, 0, 0);
        goto Exit;
        }

    for (offset = VX_FDT_CHILD(offset); offset > 0;
         offset = VX_FDT_PEER(offset))
        {
        pCur = NULL;

        if (vxbDevCreate (&pCur) != OK)
            {
            continue;
            }

        pSdioDevInfo = (SDIO_DEV_INFO *) vxbMemAlloc (sizeof (*pSdioDevInfo));

        if (pSdioDevInfo == NULL)
            {
            (void) vxbDevDestroy (pCur);
            continue;
            }

        pFdtDev = &pSdioDevInfo->vxbFdtDev;

        vxbFdtDevSetup (offset, pFdtDev);

        vxbDevNameSet (pCur, pFdtDev->name, FALSE);

        if (vxbResourceInit (&pSdioDevInfo->vxbResList) != OK)
            {
            (void) vxbDevDestroy (pCur);
            vxbMemFree (pSdioDevInfo);
            continue;
            }

        vxbDevIvarsSet (pCur, (void *) pSdioDevInfo);

        vxbDevClassSet (pCur, VXB_BUSID_SDIO);

        if (vxbFdtRegGet (&pSdioDevInfo->vxbResList, pFdtDev) != OK)
            {
            (void) vxbDevDestroy (pCur);
            vxbMemFree (pSdioDevInfo);
            continue;
            }

        if (vxbDevAdd (pDev, pCur) != OK)
            {
            (void) vxbDevDestroy (pCur);
            vxbMemFree (pSdioDevInfo);
            continue;
            }
        }

    return OK;

Exit:
    if (pDrvCtrl != NULL)
        {
        if (pDrvCtrl->sdioAccessSem)
            (void)semDelete (pDrvCtrl->sdioAccessSem);
        if (pDrvCtrl->pInfo)
            vxbMemFree (pDrvCtrl->pInfo);
        vxbMemFree (pDrvCtrl);
        }

    return ERROR;
    }


/*******************************************************************************
*
* sdioBusInfoGet - Get info about SDIO function
*
* This function read advertised parameters like SD bus width, speed
* and max block size.
*
* RETURNS: OK or ERROR if SDIO bus information cannot be retrieved
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioBusInfoGet
    (
    VXB_DEV_HANDLE pDev
    )
    {
    SD_HARDWARE    *pSdHardware;
    SDIO_CTRL      *pDrvCtrl;
    SDIO_INFO      *pSdioInfo;
    UINT8           data;
    UINT16          maxBlockSize;

    pDrvCtrl = (SDIO_CTRL *)GET_DRVCTRL (pDev);
    if (pDrvCtrl == NULL)
        return ERROR;

    pSdHardware = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardware == NULL)
        return ERROR;

    pSdioInfo = vxbMemAlloc (sizeof (SDIO_INFO));

    if (pSdioInfo == NULL)
        return ERROR;

    pDrvCtrl->pInfo = pSdioInfo;

    if ( pSdHardware->isSpi) { /* This is Draeger fixed,reduced SDIO */

    	pSdioInfo->support4Bit = TRUE;
    	pSdioInfo->supportBlockMode = TRUE;
    	pSdioInfo->supportHighSpeed = TRUE;
    	pSdioInfo->maxBlockSize = 2048;

    } else {

    	/* Check if 4bits is supported */

    	if (sdioCmdIoRwDirect (pDev, SDIO_CARDCAPA_CCCR, 0, 0, &data)==ERROR)
    		return ERROR;

    	if (!(data & CCCR_CARDCAPS_LSC) ||
    			data & CCCR_CARDCAPS_4BLS)
    		pSdioInfo->support4Bit = TRUE;

    	if (data & CCCR_CARDCAPS_SMB)
    		pSdioInfo->supportBlockMode = TRUE;

    	/* Check if HIGH SPEED is supported */

    	if (sdioCmdIoRwDirect (pDev, SDIO_HIGHSEED_CCCR, 0, 0, &data) == ERROR)
    		return ERROR;

#ifdef SDIO_NO_HIGHSPEED
    	pSdioInfo->supportHighSpeed = FALSE;
#else

    	if (data & CCCR_HIGHSPEED_SHS)
    		pSdioInfo->supportHighSpeed = TRUE;
#endif

    	/* Read Card Information (CIS) */

    	if (vxbSdioReadCis (pDev) == ERROR)
    		return ERROR;

    	if ((sdioCisDataGet (pSdHardware, SDIO_CISTPL_FUNCE_CODE,\
    			FUNCE_FN_MAX_BLK_SIZE,
				sizeof (maxBlockSize), (UINT8 *)&maxBlockSize) == ERROR) ||
    			(maxBlockSize == 0) )
    		return ERROR;

    	pSdioInfo->maxBlockSize = LE_TO_CPU16(maxBlockSize);

#ifdef SDIODBG_ON

    	/*Read CCCR value*/

    	if (sdioReadCccr (pDev) == ERROR)
    	{
    		SDIO_DBG(SDIO_DBG_ERR, "sdioBusInstInit() - Read CCCR NOK\n",
    				0, 0, 0, 0, 0, 0);
    		return ERROR;
    	}
#endif
    }

    return OK;
    }


/*******************************************************************************
*
* sdioBusConfig - config SDIO bus
*
* This uction configures the SDIO function device based on the data
* advertised by the SDIO device itself in sdioBusInfoGet().
*
* RETURNS: OK or ERROR in case the SDIO bus configuration fails
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioBusConfig
    (
    VXB_DEV_HANDLE pDev
    )
    {
    SD_HARDWARE    *pSdHardware;
    SDIO_CTRL      *pDrvCtrl;
    SDIO_INFO      *pSdioInfo;
    SD_HOST_SPEC   *pHostSpec;
    UINT8           data;

    pDrvCtrl = (SDIO_CTRL *)GET_DRVCTRL (pDev);
    if (pDrvCtrl == NULL)
        return ERROR;

    pSdHardware = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardware == NULL)
        return ERROR;

    pHostSpec = (SD_HOST_SPEC *)(pSdHardware->pHostSpec);
    if (pHostSpec == NULL)
        return ERROR;

    pSdioInfo = pDrvCtrl->pInfo;


    if ( ! pSdHardware->isSpi) { /* This is Draeger fixed,reduced SDIO */

    	/* Disable card detect resistor */
    	if (sdioCmdIoRwDirect (pDev, SDIO_BUSCTRL_CCCR, 0, 0, &data) == ERROR)
    		return ERROR;

    	data |= CCCR_BUSCTRL_CD_DIS;

    	if (sdioCmdIoRwDirect (pDev, SDIO_BUSCTRL_CCCR, 0,
    			(UINT32)SDIO_DIRECT_IO_RW, &data) == ERROR)
    		return ERROR;
    }

	/* Config HIGH SPEED if supported */

    if (pSdioInfo->supportHighSpeed && pDrvCtrl->highSpeed)
        {

        if ( ! pSdHardware->isSpi) { /* This is Draeger fixed,reduced SDIO */

    		if (sdioCmdIoRwDirect (pDev, SDIO_HIGHSEED_CCCR, 0, 0, &data) == ERROR)
    			return ERROR;

    		data |= CCCR_HIGHSPEED_EHS;

    		if (sdioCmdIoRwDirect (pDev, SDIO_HIGHSEED_CCCR, 0,
    				(UINT32)SDIO_DIRECT_IO_RW, &data) == ERROR)
    			return ERROR;

        if (pHostSpec->vxbSdClkFreqSetup (pSdHardware->pHostDev,
                SDMMC_CLK_FREQ_50MHZ) == ERROR)
            {
            SDIO_DBG(SDIO_DBG_INIT, "sdioBusConfig() - Set high-speed"
                " mode NOK\n", 0, 0, 0, 0, 0, 0);
            return ERROR;
            }

        } else {

        	if (pHostSpec->vxbSdClkFreqSetup (pSdHardware->pHostDev,
        			SDMMC_CLK_FREQ_50MHZ) == ERROR)
        	{
        		SDIO_DBG(SDIO_DBG_INIT, "sdioBusConfig() - Set high-speed"
        				" mode NOK\n", 0, 0, 0, 0, 0, 0);
        		return ERROR;
        	}

        }

        }
    else
        {
        if (pHostSpec->vxbSdClkFreqSetup (pSdHardware->pHostDev,
                SDMMC_CLK_FREQ_25MHZ) == ERROR)
            {
            SDIO_DBG(SDIO_DBG_INIT, "sdioBusConfig() - Set default-speed "
                "mode NOK\n", 0, 0, 0, 0, 0, 0);
            return ERROR;
            }
        }

    /* Config bus width to 4bits if supported */

    if (pSdioInfo->support4Bit)
        {

        if ( ! pSdHardware->isSpi) { /* This is Draeger fixed,reduced SDIO */

    		if (sdioCmdIoRwDirect (pDev, SDIO_BUSCTRL_CCCR, 0, 0, &data) == ERROR)
    			return ERROR;

    		data &= ~CCCR_BUSCTRL_BUSWIDTH_MASK;
    		data |= CCCR_BUSCTRL_BUSWIDTH_4BIT;

    		if (sdioCmdIoRwDirect (pDev, SDIO_BUSCTRL_CCCR, 0,
    				(UINT32)SDIO_DIRECT_IO_RW, &data) == ERROR)
    			return ERROR;
    	}

        /* setup host to enable 4-bit bus width */

        if (pHostSpec->vxbSdBusWidthSetup != NULL)
            {
            if (pHostSpec->vxbSdBusWidthSetup (pSdHardware->pHostDev,
                    SDMMC_BUS_WIDTH_4BIT) == ERROR)
                {
                SDIO_DBG(SDIO_DBG_INIT, "sdioBusConfig() - Set 4-bit mode"
                    " NOK\n", 0, 0, 0, 0, 0, 0);
                return ERROR;
                }
            }
        }

    if ( ! pSdHardware->isSpi) { /* This is Draeger fixed,reduced SDIO */

    	if (pSdioInfo->supportBlockMode)
    	{
    		UINT16 blkSize;

    		/* hard coded blocksize instead of read size from FPGA-IP */
    		pSdioInfo->maxBlockSize = 512;
        	pDrvCtrl->maxNumBlock = 512;
    		blkSize = pSdioInfo->maxBlockSize;

    		if (sdioSetBlockSize (pDev, blkSize) == ERROR)
    			return ERROR;

    		pDrvCtrl->blkLen = blkSize;
    	}
    	else {
        	pDrvCtrl->maxNumBlock = 0;
    		pDrvCtrl->blkLen = pSdioInfo->maxBlockSize;
    	}

    } else {
    	pDrvCtrl->blkLen = pSdioInfo->maxBlockSize;
    	pDrvCtrl->maxNumBlock = 1;
    }

    return OK;
    }

/*******************************************************************************
*
* sdioSetBlockSize - set block size.
*
* This routine sets block length for an SDIO function device
*
* RETURNS: OK or ERROR in case block transfer size cannot be set properly
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioSetBlockSize
    (
    VXB_DEV_HANDLE  pDev,
    UINT16          blkSize
    )
    {
    SD_HARDWARE *pSdHardware;
    UINT8        tempValue;
    UINT8        function;

    pSdHardware = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardware == NULL)
        return ERROR;

    function = (UINT8)(pSdHardware->funcNum);

    tempValue = (UINT8)(blkSize & 0x00FF);
    if (sdioCmdIoRwDirect (pDev, SDIO_FBR_BASE(function) + SDIO_FBR_BLKSIZE, 0,
            (UINT32)SDIO_DIRECT_IO_RW, &tempValue) == ERROR)
        return ERROR;

    tempValue = (UINT8)((blkSize & 0xFF00)>>8);
    if (sdioCmdIoRwDirect (pDev, SDIO_FBR_BASE(function) + SDIO_FBR_BLKSIZE + 1,
            0, (UINT32)SDIO_DIRECT_IO_RW, &tempValue) == ERROR)
        {
            SDIO_DBG(SDIO_DBG_INIT, "sdioSetBlockLength() - Set block length"\
                " NOK\n", 0, 0, 0, 0, 0, 0);
            return ERROR;
        }

 	SDIO_DBG(SDIO_DBG_INIT, "sdioSetBlockSize(%d) - Set block size successful!\n",
        blkSize, 0, 0, 0, 0, 0);

	return OK;
    }




/*******************************************************************************
*
* sdioReadMultiBytesFromDevice - read multiple bytes from SDIO
*
* This routine reads multiple bytes from SDIO.
*
* RETURNS: OK or ERROR if the read operation fails
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioReadMultiBytesFromDevice
    (
    VXB_DEV_HANDLE  pDev,
    UINT8          *pBuffer,
    UINT16          length,
    UINT32          offset
    )
    {
    STATUS      rc;
    UINT8      *pBounceBuffer;
    UINT8      *pBuf = pBuffer;
    BOOL        isCacheAligned;
    BOOL        isCacheSafe;
    SDIO_CTRL  *pDrvCtrl;

    if (pDev == NULL)
        return ERROR;
    pDrvCtrl = vxbDevSoftcGet (pDev);
    if (pDrvCtrl == NULL)
        return ERROR;

    /* The SDHC driver does a poor cache management via vxbDmaBufLib :
	 * 1) When data buffer is not cache-aligned it always does
     * bounce-buffering in a non-cached bounce buffer. While this
	 * would make sense for reading it is a waste of CPU cycles when
	 * writing
	 * 2) When data buffer is cache-aligned, the driver does nothing,
	 * as if the data buffer were not-cached. This may be true when
     * the buffers are comming from XBD (as it happens in the case of
     * SD and MMC storage drivers) but this is not the case for SDIO.
     *
     * To cope with this inconsistent cache management in the SDHC driver,
	 * The SDIO bus driver does cache handling itself only when buffers are
	 * cache aligned. There is no need to do any cache handling for
     * non-aligned buffers as bounce-buffering is done anyway.
	 * For the case of reading (i.e. DMA from device to RAM), it is also
	 * important to know whether the buffer contains an exact number of
     * cache lines. If so, invalidation is safe and reception can be done
     * directly in the input buffer.
     * If not, invalidation is not safe and bounce buffering is required.
     * I.e. reception is done in a non-cached buffer and after DMA is complete
     * its content is tranferred in the input (cached) buffer.
	 */

    isCacheAligned = ((((UINT32)pBuf) & (_CACHE_ALIGN_SIZE - 1)) == 0);
    isCacheSafe = ((((UINT32)length) & (_CACHE_ALIGN_SIZE - 1)) == 0);

    if (isCacheAligned)
        {
        if (isCacheSafe)
            (void)cacheInvalidate (DATA_CACHE, pBuf, length);
        else
            {

            /* do bounce buffering */

            if ((pBounceBuffer = cacheDmaMalloc (length)) == NULL)
                return ERROR;

            pBuf = pBounceBuffer;
            }
        }

    if (semTake (pDrvCtrl->sdioAccessSem, WAIT_FOREVER) != OK)
        return ERROR;

    rc =  sdioTransferBytes (pDev, pBuf, length, offset, FALSE);
    if (semGive (pDrvCtrl->sdioAccessSem) != OK)
        return ERROR;

    if (isCacheAligned)
        {

        /* Cache invalidation is also  necessary after DMA is complete
		 * due to speculative cache prefetching that is aggresive
		 * particularly on ARM Cortex platforms.
		 */

        if (isCacheSafe)
            (void)cacheInvalidate (DATA_CACHE, pBuf, length);
        else
            {
            bcopy (pBuf, pBuffer, length);
            rc = cacheDmaFree (pBuf);
            }
        }

    return rc;
    }


/*******************************************************************************
*
* sdioWriteMultiBytesToDevice - write multiple bytes to SDIO.
*
* This routine write multiple bytes to SDIO.
*
* RETURNS: OK or ERROR in case the write operation fails
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioWriteMultiBytesToDevice
    (
    VXB_DEV_HANDLE  pDev,
    UINT8          *pBuffer,
    UINT16          length,
    UINT32          offset
    )
    {
    SDIO_CTRL  *pDrvCtrl;
    STATUS      rc;

    if (pDev == NULL)
        return ERROR;
    pDrvCtrl = vxbDevSoftcGet (pDev);
    if (pDrvCtrl == NULL)
        return ERROR;

	/* There is no need to do any cache handling for cache-nonaligned buffers
     * as bounce-buffering is done anyway in the SDHC driver via vxbDmaBufLib.
     */

    if ( ((UINT32)pBuffer & (_CACHE_ALIGN_SIZE - 1)) == 0)
        if ((rc = cacheFlush (DATA_CACHE, pBuffer, (size_t)length)) != OK)
            return rc;

    if (semTake (pDrvCtrl->sdioAccessSem, WAIT_FOREVER) != OK)
        return ERROR;

    rc = sdioTransferBytes (pDev, pBuffer, length, offset, TRUE);
    if (semGive (pDrvCtrl->sdioAccessSem) != OK)
        return ERROR;

    return rc;
    }


/*******************************************************************************
*
* sdioTransferBytes - transfer multiple bytes to/from SDIO using CMD53
*
* This routine reads or writes  multiple bytes to SDIO using
* IO_RW_EXTENDED command (i.e. CMD53).
* It does this in Block mode (if supported) for a number of bytes that
* is multiple of maximum block size. For the remaining bytes (less than
* max block size) it does the SDIO access in Byte mode
* (see SDIO_DIRECT_IOEXT_BLK bit).
*
* RETURNS: OK or ERROR in case the transfer fails
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioTransferBytes
    (
    VXB_DEV_HANDLE  pDev,
    UINT8          *buffer,
    UINT16          length,
    UINT32          offset,
    BOOL            doWrite
    )
    {
    SDIO_CTRL      *pDrvCtrl;
    SD_HARDWARE    *pSdHardWare;
    SDDATA          sdioData;
    ULONG           blocks;
    ULONG           bytes;
    UINT8          *pData = buffer;
    UINT16 blkSize;
    UINT32          arg;

    pDrvCtrl = (SDIO_CTRL *)GET_DRVCTRL(pDev);
    if (pDrvCtrl == NULL)
        return ERROR;

    pSdHardWare = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardWare == NULL)
        return ERROR;

    blkSize = (UINT16)(pDrvCtrl->blkLen);

    if ( pDrvCtrl->maxNumBlock > 0) {
        blocks = length / blkSize;
        bytes = length % blkSize;
    } else {
        /* the reduced function set does not support block mode (yet?) */
        blocks = 0;
        bytes = length;
    }

    SDIO_DBG(SDIO_DBG_RW, "sdioTransferBytes() -  length: %d, blocks = %d, "\
        "bytes = %d offSet = 0x%x\n", length, blocks, bytes, offset, 0, 0);

    arg = (doWrite ? SDIO_DIRECT_IO_RW : 0x0);

    /* SDIO accesses are done at incrementing addresses */

    arg |= SDIO_DIRECT_IOEXT_OP;

    /*Send in block mode*/

    while (blocks > 0)
        {
        sdioData.buffer = pData;
        sdioData.blkNum = min(blocks, pDrvCtrl->maxNumBlock);
        sdioData.blkSize = blkSize;
		if (sdioCmdIoRwExtend(pDev, offset, pSdHardWare->funcNum,
                arg | SDIO_DIRECT_IOEXT_BLK,
                &sdioData) == ERROR)
            {
            SDIO_DBG(SDIO_DBG_ERR, "sdioTransferBytes() in block mode "\
                "failed!\n", sdioData.blkNum, 0, 0, 0, 0, 0);
            return ERROR;
            }

        pData += sdioData.blkNum * blkSize;
        offset += sdioData.blkNum * blkSize;
        blocks -= sdioData.blkNum;
        }

    /*Send in extended byte mode*/
    while (bytes >= 4)
    {
    	sdioData.buffer = pData;
    	sdioData.blkSize = min(min(512, blkSize), bytes);
    	sdioData.blkSize &= ~3;
    	sdioData.blkNum = 1;
    	if (sdioCmdIoRwExtend(pDev, offset, pSdHardWare->funcNum, arg,
    			&sdioData) == ERROR)
    	{
    		SDIO_DBG(SDIO_DBG_ERR, "sdioTransferBytes() in extended byte mode failed!\n",
    				0, 0, 0, 0, 0, 0);
    		return ERROR;
    	}

    	pData += sdioData.blkSize;
    	bytes -= sdioData.blkSize;
    	offset += sdioData.blkSize;
    }

    /*Send in byte mode*/
    while (bytes > 0)
    {
    	if (sdioCmdIoRwDirect (pDev, offset, pSdHardWare->funcNum, arg,
    			pData) == ERROR)
    	{
    		SDIO_DBG(SDIO_DBG_ERR, "sdioTransferBytes() in byte mode failed!\n",
    				0, 0, 0, 0, 0, 0);
    		return ERROR;
    	}

    	SDIO_DBG(SDIO_DBG_RW, "sdioTransferBytes: sdioCmdIoRwDirect off=0x%0x, left=%d, %02x\n",
    			offset, bytes, *pData, 0, 0, 0);

    	pData  ++;
    	bytes  --;
    	offset ++;
    }


    SDIO_DBG(SDIO_DBG_RW, "sdioTransferBytes() - SUCCESS!\n",
        0, 0, 0, 0, 0, 0);

    return OK;
    }

/*******************************************************************************
*
* sdioReadByte - read one byte from SDIO function
*
* This routine reads one byte from SDIO function
*
* RETURNS: OK or ERROR if one byte could not be read
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioReadByte
    (
    VXB_DEV_HANDLE pDev,
    UINT32         offset,
    UINT8         *pData
    )
    {
    SD_HARDWARE * pSdHardWare;

    pSdHardWare = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardWare == NULL)
        {
        SDIO_DBG (SDIO_DBG_ERR, "sdioReadByte() - pSdHardWare is NULL\n",
                    0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    return sdioCmdIoRwDirect (pDev, offset, pSdHardWare->funcNum, 0, pData);
    }


/*******************************************************************************
*
* sdioReadWord - read one 16-bit word from SDIO function
*
* This routine reads one 16-bit word from SDIO function.
* It does this by reading each byte individually (i.e. via CMD52)
* and then concatenating them into a 16-bit word.
* The reason it uses CMD52 (instead of the more efficient CMD53) is that
* CMD53 is an optional feature whereas CMD52 is mandatory.
*
* RETURNS: OK or ERROR if two bytes could not be read
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioReadWord
    (
    VXB_DEV_HANDLE pDev,
    UINT32         offset,
    UINT16        *pData
    )
    {
    STATUS          ret;
    SD_HARDWARE    *pSdHardWare;

    pSdHardWare = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardWare == NULL)
        {
        SDIO_DBG (SDIO_DBG_ERR, "sdioReadWord() - pSdHardWare is NULL\n",
                    0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    uint8_t * pBuff;
    pBuff = (uint8_t *)cacheDmaMalloc (2);
    if (pBuff == NULL)
        return ERROR;
    ret = sdioTransferBytes (pDev, pBuff, 2, offset, 0);
    *pData = LE_TO_CPU16(*(UINT16 *)pBuff);
    free (pBuff);


    return ret;
    }


/*******************************************************************************
*
* sdioReadLong - read one UINT32 from a SDIO function
*
* This routine reads one UINT32 from a SDIO function.
* It does this by reading each byte individually (i.e. via CMD52)
* and then concatenating them into a 32-bit result.
*
* RETURNS: OK or ERROR if four bytes could not be read
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioReadLong
    (
    VXB_DEV_HANDLE pDev,
    UINT32         offset,
    UINT32        *pData
    )
    {
    STATUS          ret;
    SD_HARDWARE    *pSdHardWare;

    pSdHardWare = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardWare == NULL)
        {
        SDIO_DBG (SDIO_DBG_ERR, "sdioReadWord() - pSdHardWare is NULL\n",
                    0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    uint8_t * pBuff;
    pBuff = (uint8_t *)cacheDmaMalloc (4);
    if (pBuff == NULL)
        return ERROR;
    ret = sdioTransferBytes (pDev, pBuff, 4, offset, 0);
    *pData = LE_TO_CPU32(*(UINT32 *)pBuff);
    free (pBuff);

    return ret;
    }


/*******************************************************************************
*
* sdioWriteByte - write one byte to a SDIO function
*
* This routine writes one byte to a SDIO function
*
* RETURNS: OK or ERROR if one byte could not be written
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioWriteByte
    (
    VXB_DEV_HANDLE pDev,
    UINT32         offset,
    UINT8          data
    )
    {
    SD_HARDWARE * pSdHardWare;

    pSdHardWare = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardWare == NULL)
        {
        SDIO_DBG (SDIO_DBG_ERR, "sdioReadByte() - pSdHardWare is NULL\n",
                    0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    return sdioCmdIoRwDirect (pDev, offset, pSdHardWare->funcNum,
            (UINT32)SDIO_DIRECT_IO_RW, &data);
    }


/*******************************************************************************
*
* sdioWriteWord - write one 16-bit word to an SDIO function
*
* This routine writes one 16-bit word to a SDIO function.
* It does this by writing each byte individually (i.e. via CMD52).
*
* RETURNS: OK or ERROR if two bytes could not be written
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioWriteWord
    (
    VXB_DEV_HANDLE pDev,
    UINT32         offset,
    UINT16         data
    )
    {
    STATUS          ret;
    SD_HARDWARE    *pSdHardWare;

    pSdHardWare = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardWare == NULL)
        {
        SDIO_DBG (SDIO_DBG_ERR, "sdioReadByte() - pSdHardWare is NULL\n",
                    0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    uint8_t * pBuff;
    pBuff = (uint8_t *)cacheDmaMalloc (2);
    if (pBuff == NULL)
        return ERROR;
    *(UINT16 *)pBuff = CPU_TO_LE16(data);
    ret = sdioTransferBytes (pDev, pBuff, 2, offset, 1);
    free (pBuff);


	return ret;
    }


/*******************************************************************************
*
* sdioWriteLong - write one 32-bit long to a SDIO function
*
* This routine writes one 32-bit long to a SDIO function.
* It does this by writing each byte individually (i.e. via CMD52).
*
* RETURNS: OK or ERROR if four bytes could not be written
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioWriteLong
    (
    VXB_DEV_HANDLE pDev,
    UINT32         offset,
    UINT32         data
    )
    {
    STATUS          ret;
    SD_HARDWARE    *pSdHardWare;
    uint8_t        *pBuff;

    pSdHardWare = (SD_HARDWARE *)GET_HARDWAREINFO(pDev);
    if (pSdHardWare == NULL)
        {
        SDIO_DBG (SDIO_DBG_ERR, "sdioReadByte() - pSdHardWare is NULL\n",
                    0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    pBuff = (uint8_t *)cacheDmaMalloc (4);
    if (pBuff == NULL)
        return ERROR;
    *(UINT32 *)pBuff = CPU_TO_LE32(data);
    ret = sdioTransferBytes (pDev, pBuff, 4, offset, 1);
    free (pBuff);

	return ret;
    }


/*******************************************************************************
*
* accessSdio_0 - trampoline function used as vxBus access handle
*
* This function is the (void *) handle that is passed to SDIO child
* devices when they ask for a VXB_RES_MEMORY resource. This handle will
* be subsequently passed as first argument for all vxbRead & vxbWrite
* variants:
*    vxbRead8(), vxbRead16(), vxbRead32()
*    vxbWrite8(), vxbWrite16(), vxbWrite32()
*
* All it does is calling the actual SDIO access function (accessSdioOffset() )
* with the correct VXB_DEV_HANDLE of the SDIO bus device.
*
* RETURNS: In case of read accesses, it returns read value
*          In case of write accesses, returned value has no meaning.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT32 accessSdio_0
    (
	UINT32 handle,
	UINT32 sdioOffset,
    UINT32 value
    )
    {
    VXB_DEV_HANDLE pdev = vxbSdioAccess[0].pdev;

    return accessSdioOffset (handle, sdioOffset, value, pdev);
    }


/*******************************************************************************
*
* accessSdio_1 - trampoline function used as vxBus access handle
*
* This function is the (void *) handle that is passed to SDIO child
* devices when they ask for a VXB_RES_MEMORY resource. This handle will
* be subsequently passed as first argument for all vxbRead & vxbWrite
* variants:
*    vxbRead8(), vxbRead16(), vxbRead32()
*    vxbWrite8(), vxbWrite16(), vxbWrite32()
*
* All it does is calling the actual SDIO access function (accessSdioOffset() )
* with the correct VXB_DEV_HANDLE of the SDIO bus device.
*
* RETURNS: In case of read accesses, it returns read value
*          In case of write accesses, returned value has no meaning.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT32 accessSdio_1
    (
	UINT32 handle,
	UINT32 sdioOffset,
    UINT32 value
    )
    {
    VXB_DEV_HANDLE pdev = vxbSdioAccess[1].pdev;

    return accessSdioOffset (handle, sdioOffset, value, pdev);
    }

/*******************************************************************************
*
* accessSdioOffset - access SDIO register
*
* This function is used to read&write an SDIO register.
* The first argument specifies both the access width (8, 16 or 32 bits)
* and the access type (read or write).
*
* RETURNS: In case of read accesses, it returns read value
*          In case of write accesses, returned value has no meaning.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT32 accessSdioOffset
    (
	UINT32 handle,
	UINT32 sdioOffset,
    UINT32          value,
    VXB_DEV_HANDLE  pdev
    )
    {
    UINT32 data = 0;

    SDIO_CTRL *pDrvCtrl;
    pDrvCtrl = vxbDevSoftcGet (pdev);

    if (semTake (pDrvCtrl->sdioAccessSem, WAIT_FOREVER) != OK)
        return 0;

    if (VXB_HANDLE_OP(handle) == VXB_HANDLE_OP_READ)
        {

        /* read access */

        switch VXB_HANDLE_WIDTH(handle)
            {
            case 1:
                (void) sdioReadByte (pdev, sdioOffset, (UINT8 *)&data);
                break;
            case 2:
                (void) sdioReadWord (pdev, sdioOffset, (UINT16 *)&data);
                break;
            case 4:
            default:
                (void) sdioReadLong (pdev, sdioOffset, &data);
                break;
            }
        }
    else /* VXB_HANDLE_OP_WRITE */
        {

        /* write access */

        switch VXB_HANDLE_WIDTH(handle)
            {
            case 1:
                (void) sdioWriteByte (pdev, sdioOffset, value & 0xff);
                break;
            case 2:
                (void) sdioWriteWord (pdev, sdioOffset, value & 0xffff);
                break;
            case 4:
            default:
                (void) sdioWriteLong (pdev, sdioOffset, value);
                break;
            }
        }
    if (semGive (pDrvCtrl->sdioAccessSem) != OK)
        return 0;

    return data;
    }


/*******************************************************************************
*
* sdioResAlloc - provides a resource for a child device
*
* This routine provides one resource for one child device.
* In the particular case of SDIO, only VXB_RES_MEMORY resources
* are provided.
*
* RETURNS: pointer to allocated resource
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VXB_RESOURCE * sdioResAlloc
    (
    VXB_DEV_HANDLE  pDev,
    VXB_DEV_HANDLE  pChild,
    UINT32          id
    )
    {
    SDIO_CTRL          *pDrvCtrl;
    SDIO_DEV_INFO      *pSdioDevInfo;
    VXB_RESOURCE       *pVxbRes;
    VXB_RESOURCE_ADR   *pVxbAdrRes;

    pDrvCtrl = (SDIO_CTRL *)GET_DRVCTRL (pDev);
    if (pDrvCtrl == NULL)
        return NULL;

    pSdioDevInfo = (SDIO_DEV_INFO *) vxbDevIvarsGet (pChild);

    if (pSdioDevInfo == NULL)
        {
        return NULL;
        }

    pVxbRes = vxbResourceFind (&pSdioDevInfo->vxbResList, id);

    if (pVxbRes == NULL)
        {
        return NULL;
        }

    if (VXB_RES_TYPE (pVxbRes->id) == VXB_RES_MEMORY)
        {
        pVxbAdrRes = pVxbRes->pRes;

        if (pVxbAdrRes->size == 0)
            {
            SDIO_DBG(SDIO_DBG_ERR, "sdioResAlloc() - resource size is zero\n",
                0, 0, 0, 0, 0, 0);
            return NULL;
            }

        pVxbAdrRes->virtual = (VIRT_ADDR) pVxbAdrRes->start;
        pVxbAdrRes->pHandle = vxbSdioAccess[pDrvCtrl->idx].busAccess;

        return pVxbRes;
        }
    else
        return NULL;

    }


/*******************************************************************************
*
* sdioResFree - frees one resource for a child device
*
* This routine frees one resource allocated by calling sdioResAlloc()
*
* RETURNS: OK if free successfully, otherwise ERROR
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioResFree
    (
    VXB_DEV_HANDLE      pDev,
    VXB_DEV_HANDLE      pChild,
    VXB_RESOURCE       *pRes
    )
    {
    (void) pDev;
    SDIO_DEV_INFO * pSdioDevInfo;
    pSdioDevInfo = (SDIO_DEV_INFO *) vxbDevIvarsGet (pChild);

    if (pSdioDevInfo == NULL)
        {
        return ERROR;
        }

    if (VXB_RES_TYPE(pRes->id) != VXB_RES_MEMORY)
        return ERROR;

    return OK;
    }


/*******************************************************************************
*
* sdioResListGet - get the resource list of specific device
*
* This routine provides the resource list for an SDIO child device
*
* RETURNS: resource list pointer
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VXB_RESOURCE_LIST * sdioResListGet
    (
    VXB_DEV_HANDLE pDev,
    VXB_DEV_HANDLE pChild
    )
    {
    (void) pDev;
    SDIO_DEV_INFO * pSdioDevInfo;
    pSdioDevInfo = (SDIO_DEV_INFO *) vxbDevIvarsGet (pChild);

    if (pSdioDevInfo == NULL)
        {
        return NULL;
        }

    return &pSdioDevInfo->vxbResList;
    }


/*******************************************************************************
*
* sdioDevInfo - gets the sdio child FDT device information
*
* This routine gets the sdio  child device FDT information
*
* RETURNS: the device information pointer
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VXB_FDT_DEV * sdioDevInfo
    (
    VXB_DEV_HANDLE pDev,
    VXB_DEV_HANDLE pChild
    )
    {
    (void) pDev;
    SDIO_DEV_INFO * pSdioDevInfo;

    if (pChild == NULL)
        {
        return NULL;
        }

    pSdioDevInfo = vxbDevIvarsGet (pChild);

    if (pSdioDevInfo == NULL)
        {
        return NULL;
        }

    return &pSdioDevInfo->vxbFdtDev;
    }

/*******************************************************************************
*
* sdioIdxAlloc - allocates a global card index
*
* This routine allocates a global card index.
*
* RETURNS: OK or ERROR if an idex could not be allocated
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioIdxAlloc
    (
    SDIO_CTRL * pDrvCtrl
    )
    {
    UINT32     *pCardIdx;
    UINT8       idx;

    pCardIdx = &sdioDevIdx;

    for (idx = 0; idx < MAX_SDIO_BUSES; idx++)
        {
        if (!((*pCardIdx) & (1 << idx)))
            {
            (*pCardIdx) |= (1 << idx);
            break;
            }
        }

    if (idx == MAX_SDIO_BUSES)
        {
        return ERROR;
        }
    else
        {
        pDrvCtrl->idx = idx;
        return OK;
        }
    }


/*******************************************************************************
*
* sdioCisDataGet - read SDIO tuple information
*
* This function traverses all tuples stored for each
* SDIO function device and gets the tuple data for a specific
* tuple code (if found in the list).
* See generic SD/MMC library method vxbSdioReadCis().
*
* RETURNS: OK or ERROR if CIS information could not be retrieved
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS sdioCisDataGet
    (
    SD_HARDWARE    *pSdHardware,
    UINT8           cisTupleCode,
    UINT8           offset,
    UINT8           len,
    UINT8          *pData
    )
    {
    SDIO_FUNC_TUPLE *pTuple;

    if (pSdHardware->pSdioFuncTuple == NULL)
        {
		return ERROR;
		}

	for (pTuple = pSdHardware->pSdioFuncTuple; pTuple; pTuple = pTuple->next)
        {
        if (pTuple->code == cisTupleCode)
            break;
        }

    if (pTuple == NULL)
        {
        SDIO_DBG(SDIO_DBG_ERR, "sdioCisDataGet() - tuple %d not found\n",
            cisTupleCode, 0, 0, 0, 0, 0);
        return ERROR;
        }

    /* copy tuple data */

    (void) memcpy (pData, pTuple->pData + offset, len);

    return OK;
    }

#ifdef  SDIODBG_ON

/*******************************************************************************
 *
 * sdioReadCccr -  read cccr area for an SDIO card (function 0).
 *
 * RETURNS: OK or ERROR if CCCR data could not be retrieved
 *
 * ERRNO: N/A
 */

LOCAL STATUS sdioReadCccr
    (
    VXB_DEV_HANDLE pDev
    )
    {
    SDIO_CTRL  *pDrvCtrl;
    SDIO_INFO  *pSdioInfo;
    UINT8      *pPtr;
    UINT8       i;

    pDrvCtrl = (SDIO_CTRL *)GET_DRVCTRL (pDev);
    if (pDrvCtrl == NULL)
		{
        return ERROR;
		}
		
    pSdioInfo = pDrvCtrl->pInfo;
    if (pSdioInfo == NULL)
        {
        return ERROR;
		}

    pPtr = (UINT8 *)&(pSdioInfo->cccr);

    for (i = 0; i < sizeof (SDIO_CCCR);i++)
        {
        if (sdioCmdIoRwDirect (pDev, i, 0, 0, pPtr) == ERROR)
            {
            SDIO_DBG(SDIO_DBG_ERR, "sdioReadCccr(): read failed at %d bytes\n!",
                i+1, 0, 0, 0, 0, 0);
            break;
            }

        pPtr++;
        }

    /*Print out CCCR value*/

    PRINT_CCCR_VALUE(cccr_sdio_revision);
    PRINT_CCCR_VALUE(sd_specification_revision);
    PRINT_CCCR_VALUE(io_enable);
    PRINT_CCCR_VALUE(io_ready);
    PRINT_CCCR_VALUE(int_enable);
    PRINT_CCCR_VALUE(int_pending);
    PRINT_CCCR_VALUE(io_abort);
    PRINT_CCCR_VALUE(bus_interface_control);
    PRINT_CCCR_VALUE(card_capability);
    PRINT_CCCR_VALUE(common_cis_pointer_low);
    PRINT_CCCR_VALUE(common_cis_pointer_middle);
    PRINT_CCCR_VALUE(common_cis_pointer_high);
    PRINT_CCCR_VALUE(bus_suspend);
    PRINT_CCCR_VALUE(function_select);
    PRINT_CCCR_VALUE(exec_flags);
    PRINT_CCCR_VALUE(ready_flags);
    PRINT_CCCR_VALUE(fn0_block_size_low);
    PRINT_CCCR_VALUE(fn0_block_size_high);
    PRINT_CCCR_VALUE(power_control);
    PRINT_CCCR_VALUE(bus_speed_select);
    PRINT_CCCR_VALUE(uhs_i_support);
    PRINT_CCCR_VALUE(driver_strength);
    PRINT_CCCR_VALUE(interrupt_extension);

    return OK;
}
#endif
